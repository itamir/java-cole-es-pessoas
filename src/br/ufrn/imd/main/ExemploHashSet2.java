package br.ufrn.imd.main;

import java.util.HashSet;

import br.ufrn.imd.dominio.Pessoa;

public class ExemploHashSet2 {

	public static void main(String[] args) {

		Pessoa p1 = new Pessoa("111", "Itamir");
		Pessoa p2 = new Pessoa("222", "Paul");
		Pessoa p3 = new Pessoa("222", "Mike");
		Pessoa p4 = new Pessoa("111", "Itamir");		
		
		HashSet<Pessoa> pessoas = new HashSet<Pessoa>();
		pessoas.add(p1);
		pessoas.add(p2);
		pessoas.add(p3);
		pessoas.add(p4);
		
		for (Pessoa pessoa : pessoas) {
			System.out.println(pessoa);
		}
		
		System.out.println(pessoas.size());
		
		if(pessoas.contains(p1))
			System.out.println("Possui p1.");
	}

}
