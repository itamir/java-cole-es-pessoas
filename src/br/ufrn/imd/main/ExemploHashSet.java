package br.ufrn.imd.main;

import java.util.HashSet;

public class ExemploHashSet {

	public static void main(String[] args) {
		
		HashSet<String> set = new HashSet<String>();
		set.add("Itamir");
		set.add("John");
		set.add("Paul");
		set.add("Paul");
		set.add("John");
		set.add("John");
		set.add("John");
		set.add("John");
		
		for (String item : set) {
			System.out.println(item);
		}
		
	}
	
}
