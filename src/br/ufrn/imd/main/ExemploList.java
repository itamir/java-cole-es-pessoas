package br.ufrn.imd.main;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import br.ufrn.imd.dominio.Pessoa;

public class ExemploList {

	public static void main(String[] args) {

		Pessoa p1 = new Pessoa("111", "Itamir");
		Pessoa p2 = new Pessoa("222", "Paul");
		Pessoa p3 = new Pessoa("222", "Mike");
		Pessoa p4 = new Pessoa("111", "Itamir");		
		
		List<Pessoa> pessoas = new ArrayList<Pessoa>();
		pessoas.add(p1);
		pessoas.add(p2);
		pessoas.add(p3);
		pessoas.add(p4);
		
		String exibicao = "Total: " + pessoas.size() + "\n";
		for (Pessoa pessoa : pessoas) {
			exibicao += pessoa + "\n";
		}
		JOptionPane.showMessageDialog(null, exibicao);
	}

}
